import react, { useEffect, useState } from "react";
import "./details.css";


const Details = ({info}) => {

    const [infoData, setInfoData] = useState();
    const [isLoading, setLoading] = useState(false);


    useEffect(() =>{
        info && loadListData() && setLoading(true);
    }, [info?.id]);


    const loadListData = () => {
        
        return fetch(`https://raw.githubusercontent.com/netology-code/ra16-homeworks/master/hooks-context/use-effect/data/${info.id}.json`)
        .then(res => res.json())
        .then(res => {
            setInfoData(res); 
            setLoading(false);
        })
    }


    if (isLoading) return <div className="details"> ...Loading</div>;
    if (infoData) {
        return (
            <div key={infoData.id} className="details">
                <div className="details-items">
                    <div><img src={infoData.avatar} alt={infoData.avatar}></img></div>
                    <div className="details-item">{infoData.name}</div>
                    <div className="details-item">City: {infoData.details.city}</div>
                    <div className="details-item">Company: {infoData.details.company}</div>
                    <div className="details-item">Position: {infoData.details.position}</div>
                </div>
            </div>
        );
    }
    else 
        return null;
}

export default Details;