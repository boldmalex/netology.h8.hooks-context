import react, { useEffect, useState } from "react";
import "./list.css";

const List = ({onClick}) => {

    const [items, setItems] = useState([]);

    const click = (item) => {
        onClick(item);
    }

    useEffect(() => {
        loadListData();
    }, []);

    const loadListData = () => {
        return fetch("https://raw.githubusercontent.com/netology-code/ra16-homeworks/master/hooks-context/use-effect/data/users.json")
        .then(res => res.json())
        .then(res => setItems(res));
    }

    const showItems = items.map(item => {
        return (
            <div key={item.id}>
                <ListItem {...item} onClick={click}/>
            </div>
        );
    });

    return (
        <div className="list">
            {showItems}
        </div>
    );
}




const ListItem = (props) => {

    const {id, name, onClick} = props;

    const click = () =>{
        onClick({id,name});
    }

    return (
        <div className="list-item" onClick={click}>
            {props.name}
        </div>
    );
}


export default List;