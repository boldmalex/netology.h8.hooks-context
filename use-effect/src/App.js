import react, { useState } from "react";
import Details from "./components/details/details";
import List from "./components/list/list";
import "./index.css";

function App() {

  const [item, setItem] = useState();

  const click = (item) => {
    setItem(item);
  }

  return (
    <div className="main">
       <List onClick={click}/>
       <Details info={item}/>
     </div>
  );
}

export default App;
