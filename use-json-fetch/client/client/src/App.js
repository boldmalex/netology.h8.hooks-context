import react from "react";
import "./app.css";
import News from "./components/news.js";
import Data from "./components/data.js";
import Error from "./components/error.js";
import Loading from "./components/loading.js";

function App() {
  return (
    <div>
      <div className="red-content">
        <News/>
      </div>
      
      <div className="green-content">
        <Data/>
      </div>

      <div className="blue-content">
        <Error/>
      </div>

      <div className="orange-content">
        <Loading/>
      </div>

    </div>
  );
}

export default App;
