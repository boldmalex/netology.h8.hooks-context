import { useEffect, useState } from "react";

const useFetch = (url, opts) => {

    const [data, setData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    useEffect(() => {
        const fetchData = async() => {

            setLoading(true);
            setError(null);

            try {
                
                const response = await fetch(url);
                
                // Check error statuses
                if (!response.ok) {
                    switch (response.status){
                        case 404: setError('Ошибка сети');
                            break;
                        default: setError(`Ошибка ответа: ${response.status}`);
                            break;
                    }
                }

                // Check content-type
                const contentType = response.headers.get('content-type');
                if (!contentType || contentType.indexOf('application/json') === -1)
                    setError("Не JSON");

                // Get content
                const data = await response.json();
                setData(data);
                
            } catch(ex) {
                setError(ex);
            } finally {
                setLoading(false);
            }
        };

        fetchData();
    }, []);


    return [{data, loading, error}];
}

export default useFetch;