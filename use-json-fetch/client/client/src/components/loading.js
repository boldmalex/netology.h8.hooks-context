import react from "react";
import useFetch from "../hooks/use-fetch.js";

const url = "http://localhost:7070/loading";

const Loading = () => {
    const [{data, loading, error}] = useFetch(url, []);

    return (
        <div>
            <div>{url}</div>
            {loading&&<p>Loading...</p>}
            {error&&<p>{error}</p>}
            {data && <div>
                        { data.status }
                     </div>
            }
        </div>
    )
}

export default Loading;