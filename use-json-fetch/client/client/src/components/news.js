import react, {useState} from "react";
import useFetch from "../hooks/use-fetch.js";

const url = "http://localhost:7070/news/latest";

const News = () => {
    const [{data:news, loading, error}] = useFetch(url, []);

    return (
        <div>
            {url}
            {loading&&<p>Loading...</p>}
            {error&&<p>{error}</p>}
            {news && <div>
                        <ul>
                            { news.map(o => <li key = {o.id}>#{o.id}{o.content}</li>) }
                        </ul>
                    </div> 
            }
        </div>
    )
}

export default News;