const http = require('http');
const Koa = require('koa');
const Router = require('koa-router');
const cors = require('koa2-cors');
const koaBody = require('koa-body');


const app = new Koa();
app.use(cors());
app.use(koaBody());

let nextId = 1;
const latestCount = 3;
const news = [
    { id: nextId++, content: `test${nextId}` },
    { id: nextId++, content: `test${nextId}` },
    { id: nextId++, content: `test${nextId}` },
];

setInterval(()=>{
    news.push({id: nextId++, content: `test${nextId}`});
}, 1 * 1000);


const router = new Router();

router.get('/data', async (ctx, next) => {
    ctx.response.body = {status: "ok"};
});

router.get('/error', async (ctx, next) => {
    ctx.response.status = 500;
    ctx.response.body = {status: "Internal Error"};
});

router.get('/loading', async (ctx, next) => {
    await new Promise(resolve => {
        setTimeout(() => {
            resolve();
        }, 5000);
    });
    ctx.response.body = {status: "ok"};
});

router.get('/news/latest', async (ctx, next) => {
    ctx.response.body = news.slice(news.length - latestCount, news.length).reverse();
});

app.use(router.routes())
app.use(router.allowedMethods());

const port = process.env.PORT || 7070;
const server = http.createServer(app.callback());
server.listen(port);